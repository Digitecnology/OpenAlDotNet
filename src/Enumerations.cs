/* 
 * OPENAL.NET: This is the portable high-level binding of the most popular audio library, OPENAL.
 * Copyright (C) 2006 Steven Rodriguez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
// created on 20/03/2006 at 19:29
using System;
using Tao.OpenAl;

namespace OpenAlDotNet
{	
	/*
	public enum SourceType
	{
		Undetermined = , 
		Static = 0,		
		Streaming = Tao.OpenAl.Al.AL_STREAMING
	};
	*/
	
	public enum SourceState
	{
		Playing = Tao.OpenAl.Al.AL_PLAYING,
		Paused = Tao.OpenAl.Al.AL_PAUSED,
		Stopped = Tao.OpenAl.Al.AL_STOPPED
	};
	
	public enum AudioFormat
	{
		Mono16bit = Tao.OpenAl.Al.AL_FORMAT_MONO16,
		Mono8bit = Tao.OpenAl.Al.AL_FORMAT_MONO8,
		Stereo16bit = Tao.OpenAl.Al.AL_FORMAT_STEREO16,
		Stereo8bit = Tao.OpenAl.Al.AL_FORMAT_STEREO8
	};
	
	
	
	public enum DistanceModel
	{
		None = Tao.OpenAl.Al.AL_NONE,
		Inverse = Tao.OpenAl.Al.AL_INVERSE_DISTANCE,
		InverseClamped = Tao.OpenAl.Al.AL_INVERSE_DISTANCE_CLAMPED
	};
	
	public enum ErrorType
	{
		NoError = Tao.OpenAl.Al.AL_NO_ERROR,
		InvalidName = Tao.OpenAl.Al.AL_INVALID_NAME,
		InvalidEnum = Tao.OpenAl.Al.AL_INVALID_ENUM,
		InvalidValue = Tao.OpenAl.Al.AL_INVALID_VALUE,
		InvalidOperation = Tao.OpenAl.Al.AL_INVALID_OPERATION,
		OutOfMemory = Tao.OpenAl.Al.AL_OUT_OF_MEMORY
	};
}