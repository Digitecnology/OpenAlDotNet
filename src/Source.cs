/* 
 * OPENAL.NET: This is the portable high-level binding of the most popular audio library, OPENAL.
 * Copyright (C) 2006 Steven Rodriguez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
// created on 20/03/2006 at 19:30
using System;
using Tao.OpenAl;

namespace OpenAlDotNet
{
	public class Source
	{		
		//SOURCE ID
		private int sourceID;		
		
		//-----------------------------------------------------------------------
		//CONSTRUCTORS
		//-----------------------------------------------------------------------
		
		//FIXED: PREVIEW FIXING.
		public Source(OpenAlDotNet.Buffer buffer)
		{				
			Tao.OpenAl.Al.alGenSources(1,out sourceID);
			Tao.OpenAl.Al.alSourcei(sourceID,Tao.OpenAl.Al.AL_BUFFER,buffer.ID());					
		}
		
		//FIXED: PREVIEW FIXING.
		public Source()
		{
			Tao.OpenAl.Al.alGenSources(1,out sourceID);
		}
		
		//FIXED: PREVIEW FIXING.
		internal int ID()
		{			
			return sourceID;							
		}
		
		//-----------------------------------------------------------------------
		//METHODS
		//-----------------------------------------------------------------------
		
		//FIXED: PREVIEW FIXING.
		public void Play()
		{
			Tao.OpenAl.Al.alSourcePlay(sourceID);
		}
		
		//FIXED: PREVIEW FIXING.
		public void Stop()
		{
			Tao.OpenAl.Al.alSourceStop(sourceID);
		}
		
		//FIXED: PREVIEW FIXING.
		public void Pause()
		{
			Tao.OpenAl.Al.alSourcePause(sourceID);
		}
		
		//FIXED: PREVIEW FIXING.
		public void Rewind()
		{		
			Tao.OpenAl.Al.alSourceRewind(sourceID);
		}
		
		//FIXED: PREVIEW FIXING.
		//BUG: THIS IS WRONG!!!
		public void QueueBuffer(OpenAlDotNet.Buffer buffer)
		{
			int i = buffer.ID();
			Tao.OpenAl.Al.alSourceQueueBuffers(sourceID,1,ref i);
		}
		
		//FIXED: PREVIEW FIXING.
		//BUG: THIS IS WRONG!!!
		public OpenAlDotNet.Buffer[] GetBuffersProcessed()
		{			
			int temp1 = 0;
			OpenAlDotNet.Buffer[] temp2 = new OpenAlDotNet.Buffer[this.BuffersProcessed];
			
			for(int i = 0; i <= temp2.Length - 1; i++)
			{				
				Tao.OpenAl.Al.alSourceUnqueueBuffers(sourceID,1,ref temp1);
				temp2[i] = new OpenAlDotNet.Buffer(temp1);
			}
			
			return temp2;
		}
		
		//FIXED: PREVIEW FIXING.
		public void Close()
		{
			Tao.OpenAl.Al.alDeleteSources(1,ref sourceID);
		}		
	
		
		//-----------------------------------------------------------------------
		//PROPERTIES
		//-----------------------------------------------------------------------
		
		//FIXED: PREVIEW FIXING.
		public OpenAlDotNet.Buffer SourceBuffer
		{
			get
			{
				int i;
				Tao.OpenAl.Al.alGetSourcei(sourceID,Tao.OpenAl.Al.AL_BUFFER,out i);
				return new OpenAlDotNet.Buffer(i);
			}
			set
			{				
				Tao.OpenAl.Al.alSourcei(sourceID,Tao.OpenAl.Al.AL_BUFFER,value.ID());
			}
		}
		
		//FIXED: PREVIEW FIXING.
		public OpenAlDotNet.Vector3 Position
		{
			get
			{
				float[] fv = new float[3];				
				Tao.OpenAl.Al.alGetSource3f(sourceID,Tao.OpenAl.Al.AL_POSITION,out fv[0],out fv[1],out fv[2]);
				return new Vector3(fv[0],fv[1],fv[2]);
			}
			set
			{					
				Tao.OpenAl.Al.alSource3f(sourceID,Tao.OpenAl.Al.AL_POSITION,value.X,value.Y,value.Z);
			}
		}
		
		//FIXED: PREVIEW FIXING.
		public OpenAlDotNet.Vector3 Velocity
		{
			get
			{
				float[] fv = new float[3];				
				Tao.OpenAl.Al.alGetSource3f(sourceID,Tao.OpenAl.Al.AL_VELOCITY,out fv[0],out fv[1],out fv[2]);
				return new Vector3(fv[0],fv[1],fv[2]);
			}
			set
			{				
				Tao.OpenAl.Al.alSource3f(sourceID,Tao.OpenAl.Al.AL_VELOCITY,value.X,value.Y,value.Z);
			}
		}	
		
		//FIXED: PREVIEW FIXING.
		public OpenAlDotNet.Vector3 Direction
		{
			get
			{
				float[] fv = new float[3];				
				Tao.OpenAl.Al.alGetSource3f(sourceID,Tao.OpenAl.Al.AL_DIRECTION,out fv[0],out fv[1],out fv[2]);
				return new Vector3(fv[0],fv[1],fv[2]);
			}
			set
			{				
				Tao.OpenAl.Al.alSource3f(sourceID,Tao.OpenAl.Al.AL_DIRECTION,value.X,value.Y,value.Z);
			}
		}			
		
		//FIXED: PREVIEW FIXING.
		public float Gain
		{
			get
			{
				float f;
				Tao.OpenAl.Al.alGetSourcef(sourceID,Tao.OpenAl.Al.AL_GAIN,out f);
				return f;
			}
			set
			{
				Tao.OpenAl.Al.alSourcef(sourceID,Tao.OpenAl.Al.AL_GAIN,value);
			}
		}
		
		//FIXED: PREVIEW FIXING.
		public float Pitch
		{
			get
			{
				float f;
				Tao.OpenAl.Al.alGetSourcef(sourceID,Tao.OpenAl.Al.AL_PITCH,out f);
				return f;
			}
			set
			{
				Tao.OpenAl.Al.alSourcef(sourceID,Tao.OpenAl.Al.AL_PITCH,value);
			}
		}
		
		//FIXED: PREVIEW FIXING.
		public float MaxDistance
		{
			get
			{
				float f;
				Tao.OpenAl.Al.alGetSourcef(sourceID,Tao.OpenAl.Al.AL_MAX_DISTANCE,out f);
				return f;
			}
			set
			{
				Tao.OpenAl.Al.alSourcef(sourceID,Tao.OpenAl.Al.AL_MAX_DISTANCE,value);
			}
		}
		
		//FIXED: PREVIEW FIXING.
		public float RolloffFactor
		{
			get
			{
				float f;
				Tao.OpenAl.Al.alGetSourcef(sourceID,Tao.OpenAl.Al.AL_ROLLOFF_FACTOR,out f);
				return f;
			}
			set
			{
				Tao.OpenAl.Al.alSourcef(sourceID,Tao.OpenAl.Al.AL_ROLLOFF_FACTOR,value);
			}
		}
		
		//FIXED: PREVIEW FIXING.
		public float ReferenceDistance
		{
			get
			{
				float f;
				Tao.OpenAl.Al.alGetSourcef(sourceID,Tao.OpenAl.Al.AL_REFERENCE_DISTANCE,out f);
				return f;
			}
			set
			{
				Tao.OpenAl.Al.alSourcef(sourceID,Tao.OpenAl.Al.AL_REFERENCE_DISTANCE,value);
			}
		}
		
		//FIXED: PREVIEW FIXING.
		public float MinGain
		{
			get
			{
				float f;
				Tao.OpenAl.Al.alGetSourcef(sourceID,Tao.OpenAl.Al.AL_MIN_GAIN,out f);
				return f;
			}
			set
			{
				Tao.OpenAl.Al.alSourcef(sourceID,Tao.OpenAl.Al.AL_MIN_GAIN,value);
			}
		}
		
		//FIXED: PREVIEW FIXING.
		public float MaxGain
		{
			get
			{
				float f;
				Tao.OpenAl.Al.alGetSourcef(sourceID,Tao.OpenAl.Al.AL_MAX_GAIN,out f);
				return f;
			}
			set
			{
				Tao.OpenAl.Al.alSourcef(sourceID,Tao.OpenAl.Al.AL_MAX_GAIN,value);
			}
		}
		
		//FIXED: PREVIEW FIXING.
		public float ConeInnerAngle
		{
			get
			{
				float f;
				Tao.OpenAl.Al.alGetSourcef(sourceID,Tao.OpenAl.Al.AL_CONE_INNER_ANGLE,out f);
				return f;
			}
			set
			{
				Tao.OpenAl.Al.alSourcef(sourceID,Tao.OpenAl.Al.AL_CONE_INNER_ANGLE,value);
			}
		}
		
		//FIXED: PREVIEW FIXING.
		public float ConeOuterGain
		{
			get
			{
				float f;
				Tao.OpenAl.Al.alGetSourcef(sourceID,Tao.OpenAl.Al.AL_CONE_OUTER_GAIN,out f);
				return f;
			}
			set
			{
				Tao.OpenAl.Al.alSourcef(sourceID,Tao.OpenAl.Al.AL_CONE_OUTER_GAIN,value);
			}
		}
		
		//FIXED: PREVIEW FIXING.
		public float ConeOuterAngle
		{
			get
			{
				float f;
				Tao.OpenAl.Al.alGetSourcef(sourceID,Tao.OpenAl.Al.AL_CONE_OUTER_ANGLE,out f);
				return f;
			}
			set
			{
				Tao.OpenAl.Al.alSourcef(sourceID,Tao.OpenAl.Al.AL_CONE_OUTER_ANGLE,value);
			}
		}
		
		//FIXED: PREVIEW FIXING.
		/*
		public OpenAlDotNet.SourceType Type
		{
			get
			{
				return (OpenAlDotNet.SourceType)Tao.OpenAl.Al.alGetSourcei(Tao.OpenAl.Al.AL_SOURCE_TYPE);
			}			
		}
		*/
		
		//FIXED: PREVIEW FIXING.
		public bool Looping
		{
			get
			{
				int i;
				Tao.OpenAl.Al.alGetSourcei(sourceID,Tao.OpenAl.Al.AL_LOOPING,out i);
				return System.Convert.ToBoolean(i);				
			}
			set
			{
				Tao.OpenAl.Al.alSourcei(sourceID,Tao.OpenAl.Al.AL_LOOPING,System.Convert.ToInt32(value));
			}
		}	
		
		//FIXED: PREVIEW FIXING.
		public bool SourceRelative
		{
			get
			{
				int i;
				Tao.OpenAl.Al.alGetSourcei(sourceID,Tao.OpenAl.Al.AL_SOURCE_RELATIVE,out i);
				return System.Convert.ToBoolean(i);
			}
			set
			{
				Tao.OpenAl.Al.alSourcei(sourceID,Tao.OpenAl.Al.AL_SOURCE_RELATIVE,System.Convert.ToInt32(value));
			}
		}		
		
		//FIXME: IS THIS ACCEPTABLE
		public OpenAlDotNet.SourceState State
		{
			get
			{
				int i;
				Tao.OpenAl.Al.alGetSourcei(sourceID,Tao.OpenAl.Al.AL_SOURCE_STATE,out i);
				return (OpenAlDotNet.SourceState)i;
			}
			set
			{
				Tao.OpenAl.Al.alSourcei(sourceID,Tao.OpenAl.Al.AL_SOURCE_STATE,Convert.ToInt32(value));
			}
		}
		
		//FIXED: PREVIEW FIXING.
		public int BuffersQueued
		{
			get
			{
				int i;
				Tao.OpenAl.Al.alGetSourcei(sourceID,Tao.OpenAl.Al.AL_BUFFERS_QUEUED,out i);
				return i;
			}			
		}	
		
		//FIXED: PREVIEW FIXING.
		public int BuffersProcessed
		{
			get
			{
				int i;
				Tao.OpenAl.Al.alGetSourcei(sourceID,Tao.OpenAl.Al.AL_BUFFERS_PROCESSED,out i);
				return i;
			}			
		}		
	}
}