/* 
 * OPENAL.NET: This is the portable high-level binding of the most popular audio library, OPENAL.
 * Copyright (C) 2006 Steven Rodriguez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
// created on 20/03/2006 at 22:02
using System;
using Tao.OpenAl;

namespace OpenAlDotNet
{
	public class OpenAlException : System.Exception
	{
		public OpenAlException()
		{
		}		
	}
}
