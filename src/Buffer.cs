/* 
 * OPENAL.NET: This is the portable high-level binding of the most popular audio library, OPENAL.
 * Copyright (C) 2006 Steven Rodriguez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// created on 20/03/2006 at 19:29
using System;
using Tao.OpenAl;

namespace OpenAlDotNet
{
	public class Buffer
	{			
		//BUFFER ID
		private int bufferID;
		
		//-----------------------------------------------------------------------
		//CONSTRUCTORS
		//-----------------------------------------------------------------------
		
		//FIXED: FIX PREVIEW.		
		public Buffer(string FileName)
		{
			int[] temp1 = new int[4];
			byte[] temp2;
			Tao.OpenAl.Al.alGenBuffers(1, out bufferID);			
			Tao.OpenAl.Alut.alutLoadWAVFile(FileName,out temp1[0],out temp2,out temp1[1],out temp1[2],out temp1[3]);
			Tao.OpenAl.Al.alBufferData(bufferID,temp1[0],temp2,temp1[1],temp1[2]);
			Tao.OpenAl.Alut.alutUnloadWAV(temp1[0],out temp2,temp1[0],temp1[1]);			
		}
		
		//FIXED: FIX PREVIEW.
		public Buffer(byte[] Data)
		{	
			int[] temp1 = new int[4];
			byte[] temp2;
			Tao.OpenAl.Al.alGenBuffers(1, out bufferID);			
			Tao.OpenAl.Alut.alutLoadWAVMemory(Data,out temp1[0],out temp2,out temp1[1],out temp1[2],out temp1[3]);
			Tao.OpenAl.Al.alBufferData(bufferID,temp1[0],temp2,temp1[1],temp1[2]);
			Tao.OpenAl.Alut.alutUnloadWAV(temp1[0],out temp2,temp1[0],temp1[1]);	
		}	
		
		//FIXED: FIX PREVIEW
		public Buffer()
		{
			Tao.OpenAl.Al.alGenBuffers(1, out bufferID);
		}
		
		//FIXED: FIX PREVIEW
		internal Buffer(int ID)
		{
			this.bufferID = ID;
		}
		
		//-----------------------------------------------------------------------
		//OPERATORS
		//-----------------------------------------------------------------------
		
		//BETA: CORRECT THIS!!!
		/*public bool EqualsBufferEquals(OpenAlDotNet.Buffer buffer)
		{
			if(this.bufferID == buffer.bufferID)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		*/
		public static bool operator==(Buffer x,Buffer y)
		{
			return x.bufferID == y.bufferID;
		}
	
		public static bool operator!=(Buffer x,Buffer y)
		{
			return x.bufferID != y.bufferID;
		}
		
		public override int GetHashCode()
		{
			return bufferID;
		}
		
		public override bool Equals(object obj)
		{
			if(obj == null | obj.GetType() != GetType())
			{
				return false;
			}
			else
			{
				Buffer temp = (Buffer)obj;
				return this.bufferID == temp.bufferID;
			}
		}
		
		//-----------------------------------------------------------------------
		//METHODS
		//-----------------------------------------------------------------------
		
		//FIXED: FIX PREVIEW.
		internal int ID()
		{
			return bufferID;			
		}	
		
		//FIXED: FIX PREVIEW.	
		public void Close()
		{
			Tao.OpenAl.Al.alDeleteBuffers(1,ref bufferID);
		}
		
		//FIXED: FIX PREVIEW
		public void SetData(OpenAlDotNet.AudioFormat Format,byte[] PcmData,int Size,int Frequency)
		{
			Tao.OpenAl.Al.alBufferData(bufferID,Convert.ToInt32(Format),PcmData,Size,Frequency);			
		}
		
		//-----------------------------------------------------------------------
		//PROPERTIES
		//-----------------------------------------------------------------------
				
		//FIXED: FIX PREVIEW.
		public int Frequency
		{
			get
			{
				int i;
				Tao.OpenAl.Al.alGetBufferi(bufferID,Tao.OpenAl.Al.AL_FREQUENCY,out i);
				return i;
			}
		}
		
		//FIXED: FIX PREVIEW.
		public int Size
		{
			get
			{
				int i;
				Tao.OpenAl.Al.alGetBufferi(bufferID,Tao.OpenAl.Al.AL_SIZE,out i);
				return i;
			}
		}
		
		//FIXED: FIX PREVIEW.
		public int Bits
		{
			get
			{
				int i;
				Tao.OpenAl.Al.alGetBufferi(bufferID,Tao.OpenAl.Al.AL_BITS,out i);
				return i;
			}
		}
		
		//FIXED: FIX PREVIEW.
		public int Channels
		{
			get
			{
				int i;
				Tao.OpenAl.Al.alGetBufferi(bufferID,Tao.OpenAl.Al.AL_CHANNELS,out i);
				return i;
			}
		}		
	}
}