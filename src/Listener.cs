// created on 20/03/2006 at 19:30
using System;
using Tao.OpenAl;

namespace OpenAlDotNet
{
	public sealed class Listener
	{
		//-----------------------------------------------------------------------
		//CONSTRUCTORS
		//-----------------------------------------------------------------------
		
		static Listener()
		{
			
		}
		
		//-----------------------------------------------------------------------
		//METHODS
		//-----------------------------------------------------------------------
		
		//-----------------------------------------------------------------------
		//PROPERTIES
		//-----------------------------------------------------------------------
		
		//FIXED: PREVIEW FIXING.
		public static OpenAlDotNet.Vector3 Position
		{
			get
			{
				float[] fv = new float[3];				
				Tao.OpenAl.Al.alGetListener3f(Tao.OpenAl.Al.AL_POSITION,out fv[0],out fv[1],out fv[2]);
				return new Vector3(fv[0],fv[1],fv[2]);
			}
			set
			{				
				Tao.OpenAl.Al.alListener3f(Tao.OpenAl.Al.AL_POSITION,value.X,value.Y,value.Z);
			}
		}
		
		//FIXED: PREVIEW FIXING.
		public static OpenAlDotNet.Vector3 Velocity
		{
			get
			{
				float[] fv = new float[3];				
				Tao.OpenAl.Al.alGetListener3f(Tao.OpenAl.Al.AL_VELOCITY,out fv[0],out fv[1],out fv[2]);
				return new Vector3(fv[0],fv[1],fv[2]);
			}
			set
			{
				Tao.OpenAl.Al.alListener3f(Tao.OpenAl.Al.AL_VELOCITY,value.X,value.Y,value.Z);				
			}
		}	
		
		//FIXED: PREVIEW FIXING.
		public static OpenAlDotNet.Vector3 Orientation
		{
			get
			{
				float[] fv = new float[3];				
				Tao.OpenAl.Al.alGetListener3f(Tao.OpenAl.Al.AL_ORIENTATION,out fv[0],out fv[1],out fv[2]);
				return new Vector3(fv[0],fv[1],fv[2]);
			}
			set
			{				
				Tao.OpenAl.Al.alListener3f(Tao.OpenAl.Al.AL_ORIENTATION,value.X,value.Y,value.Z);
			}
		}	
		
		//FIXED: PREVIEW FIXING.
		public static float Gain
		{
			get
			{
				float f;
				Tao.OpenAl.Al.alGetListenerf(Tao.OpenAl.Al.AL_GAIN,out f);
				return f;
			}
			set
			{
				Tao.OpenAl.Al.alListenerf(Tao.OpenAl.Al.AL_GAIN,value);
			}
		}
	}	
}