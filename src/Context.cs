/* 
 * OPENAL.NET: This is the portable high-level binding of the most popular audio library, OPENAL.
 * Copyright (C) 2006 Steven Rodriguez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
// created on 20/03/2006 at 19:30
using System;
using Tao.OpenAl;

namespace OpenAlDotNet
{
	public sealed class Context
	{
		//DEVICE AND CONTEXT ID'S
		private static System.IntPtr contextID;
		private static System.IntPtr deviceID;
		//SPECIAL VARIABLES
		private static int contextattribute;
		
		
		//-----------------------------------------------------------------------
		//CONSTRUCTORS
		//-----------------------------------------------------------------------
		
		static Context()
		{
				
		}
		
		//-----------------------------------------------------------------------
		//METHODS
		//-----------------------------------------------------------------------
		
		//FIXED: FIX PREVIEW.
		public static void Initialize(string DeviceName)
		{				
			contextattribute = Tao.OpenAl.Alc.ALC_SYNC;
			deviceID = Tao.OpenAl.Alc.alcOpenDevice(DeviceName);			
			contextID = Tao.OpenAl.Alc.alcCreateContext(deviceID,ref contextattribute);			
			Tao.OpenAl.Alc.alcMakeContextCurrent(contextID);			
		}
		
		//FIXED: FIX PREVIEW.
		public static void Close()
		{			
			Tao.OpenAl.Alc.alcDestroyContext(contextID);
			Tao.OpenAl.Alc.alcCloseDevice(deviceID);
		}
		
		//-----------------------------------------------------------------------
		//PROPERTIES
		//-----------------------------------------------------------------------
		
		//FIXED: FIX PREVIEW.
		public static string Version
		{
			get
			{				
				return Tao.OpenAl.Al.alGetString(Tao.OpenAl.Al.AL_VERSION);
			}
		}
		
		//FIXED: FIX PREVIEW.
		public static string Renderer
		{
			get
			{
				return Tao.OpenAl.Al.alGetString(Tao.OpenAl.Al.AL_RENDERER);
			}
		}
		
		//FIX: IS THIS PROPERTY ACCEPTABLE???
		public static string Extensions
		{
			get
			{				
				return Tao.OpenAl.Al.alGetString(Tao.OpenAl.Al.AL_EXTENSIONS);
			}
		}
		
		//FIXED: FIX PREVIEW.
		public static string Vendor
		{
			get
			{				
				return Tao.OpenAl.Al.alGetString(Tao.OpenAl.Al.AL_VENDOR);
			}
		}		
		
		//FIXED: FIX PREVIEW.
		public static OpenAlDotNet.DistanceModel Distance
		{
			get
			{
				return (OpenAlDotNet.DistanceModel)Tao.OpenAl.Al.alGetInteger(Tao.OpenAl.Al.AL_DISTANCE_MODEL);
			}
			set
			{
				
				Tao.OpenAl.Al.alDistanceModel(Convert.ToInt32(value));
			}
		}	
		
		//FIXED: FIX PREVIEW.
		public static int DopplerFactor
		{
			get
			{				
				return Tao.OpenAl.Al.alGetInteger(Tao.OpenAl.Al.AL_DOPPLER_FACTOR);
			}
			set
			{
				Tao.OpenAl.Al.alDopplerFactor(value);
			}
		}		
	}
}